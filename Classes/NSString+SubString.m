//
//  NSString+SubString.m
//  ZYBLive
//
//  Created by 赵文超 on 2017/12/10.
//  Copyright © 2017年 zyb. All rights reserved.
//

#import "NSString+SubString.h"

@implementation NSString (SubString)

- (NSString *)subStringWithStartString:(NSString *)startString endString:(NSString *)endString
{
    NSRange startRange = [self rangeOfString:startString];
    NSRange endRange = [self rangeOfString:endString];

    NSRange range = NSMakeRange(startRange.location, endRange.location + endRange.length - startRange.location);
    NSString *dataStr = [self substringWithRange:range];
    return dataStr;
}

@end
