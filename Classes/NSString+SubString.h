//
//  NSString+SubString.h
//  ZYBLive
//
//  Created by 赵文超 on 2017/12/10.
//  Copyright © 2017年 zyb. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (SubString)

/**
 截取两个字符串（包含该字符串）中间的字符创

 @param startString 起始字符串
 @param endString   结束字符串
 @return            截取后的字符串
 */
- (NSString *)subStringWithStartString:(NSString *)startString endString:(NSString *)endString;

@end
